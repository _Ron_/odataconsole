﻿using System;
using System.Net;
using System.Threading.Tasks;
using Simple.OData.Client;
using PostSharp.Patterns.Diagnostics;

[assembly: Log]
namespace ODataConsole
{
    [Log(AttributeExclude = true)]
    class Program
    {
        static async Task Main(string[] args)
        {
            var client = new ODataClient("https://atlas365dev2fd224fa7a1cac76devaos.cloudax.dynamics.com/data");
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var metadata = await client.GetMetadataAsync();
            
            Console.WriteLine(metadata.ToString());

            Console.ReadLine();
        }
    }
}
